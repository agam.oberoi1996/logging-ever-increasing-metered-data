//Requiring the needed packages
const mqtt = require("mqtt");
const Influx = require("influx");

//Making a schema for influx
const influx = new Influx.InfluxDB({
    host: 'localhost',
    database: 'meterData',
    username: "mellowtoga",
    password: "Hello123"
})

//Checking if database exists, if not, then create Database
influx.getDatabaseNames()
.then((names) => {
    if (!names.includes("meterData")){
        return influx.createDatabase("meterData");
    }
})

//Create MQTT client
const client = mqtt.connect([{host: 'localhost', port: 1883, clientId: 'Agamdeep'}])

//Declare metric variable
let metric = 0;
let row = {};
const topic = "reading_metered_data";

//Connecting to the MQTT client and check if connected
//Publish and subscribe to the data
client.on("connect", () => {
    console.log("MQTT Connected!")
    client.subscribe(topic);
    
})


let intervalID = setInterval(() => {
    metric += Math.round(Math.random() * 4 + 1);
    //console.log({metric});
    //Creating the data point
    row = {
        measurement: 'meteredTrial',
        tag: {sensor: "meterReading"},
        value: {reading: metric},
        timestamp: Date.now()
    }

    //Subscribing/Publishing to the string of data
    client.publish(topic, JSON.stringify(row));

}, 3000)
//End the running of the interval 
setTimeout(() => {
    clearInterval(intervalID);
    client.end();
}, 20000)
 //3,600,000 is 1 hour

 client.on("message", (topic, message) => {
    //influx.writePoints([message]);
    //influx.writePoints([JSON.parse(message)]);
    data = JSON.parse(message);
    
    influx.writePoints([{
        measurement: data.measurement,
        tags: data.tag,
        fields: data.value,
        timestamp: data.timestamp,
    },]).then(() => {
        console.log("Data saved to InfluxDB");
    }).catch(error => {
        console.log("There was an error saving data to Influx");
    })
})

//hackerrank
/*
client.on("disconnect", () => {
    influx.query(`SELECT * FROM meteredTrial`)
    .then(result => console.log(result));
})
*/